﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using Contract;

namespace Behavior
{
  public class Playback
  {
    public enum State
    {
      PlayingMainSource,
      PlayingAd,
      Finished
    }

    public class Ad
    {
      private Ad(Source source, double position)
      {
        Source = source;
        Position = position;
      }

      public Source Source { get; }
      public double Position { get; }

      public static Ad GetInstance(Source source, double position)
      {
        return new Ad(source, position);
      }
    }

    private readonly IEnumerable<Ad> ads;
    private readonly Source source;

    private readonly UI ui;
    private double currentPosition;
    private double currentSpeed;
    private Ad playingAd;
    private State state;

    private Playback(UI ui, Source source, IEnumerable<Ad> ads)
    {
      this.ui = ui;
      this.ui.PositionUpdate += OnPositionUpdate;
      this.ui.SourceFinished += OnSourceFinished;
      this.source = source;
      this.ads = ads;
      currentSpeed = 1d;
      OnPlayMainSource();
    }

    public State CurrentState => state;

    public static double BoundaryDetectionTolerance => 0.1d;

    public static Playback Start(UI ui, Source source, IEnumerable<Ad> ads)
    {
      return new Playback(ui, source, ads);
    }

    private void OnPositionUpdate(double newPosition)
    {
      switch (state)
      {
        case State.PlayingMainSource:
          PlayNextAd(currentPosition, newPosition);
          currentPosition = newPosition;
          break;
        case State.PlayingAd:
          break;
        case State.Finished:
          throw new InvalidOperationException();
      }
    }

    private void OnSourceFinished()
    {
      switch (state)
      {
        case State.PlayingAd:
          if (!PlayNextAd(playingAd.Position, currentPosition))
            OnPlayMainSource();
          break;
        case State.PlayingMainSource:
          ui.Close();
          state = State.Finished;
          break;
        case State.Finished:
          throw new InvalidOperationException();
      }
    }

    private void OnPlayMainSource()
    {
      ui.Play(source, currentPosition);
      ui.FastForwardEnabled = true;
      ui.PlaybackSpeed = currentSpeed;
      state = State.PlayingMainSource;
    }

    private bool PlayNextAd(double beforePosition, double afterPosition)
    {
      foreach (var ad in ads)
        if (beforePosition < ad.Position && afterPosition > ad.Position)
        {
          ui.Play(ad.Source, 0);
          ui.FastForwardEnabled = false;
          currentSpeed = ui.PlaybackSpeed;
          ui.PlaybackSpeed = 1;
          state = State.PlayingAd;
          playingAd = ad;
          return true;
        }

      return false;
    }
  }
}