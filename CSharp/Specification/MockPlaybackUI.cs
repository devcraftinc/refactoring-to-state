﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using Contract;

namespace Specification
{
  internal class MockPlaybackUI : UI
  {
    internal MockPlaybackUI()
    {
      PlayCalls = new List<(Source, double)>();
    }

    public List<(Source source, double position)> PlayCalls { get; }
    public int CloseCalls { get; private set; }

    public override bool FastForwardEnabled { get; set; }
    public override double PlaybackSpeed { get; set; }

    public override void Play(Source source, double seconds)
    {
      PlayCalls.Add((source, seconds));
    }

    public override void Close()
    {
      CloseCalls++;
    }

    public override event Action<double> PositionUpdate;
    public override event Action SourceFinished;

    internal void Reset()
    {
      PlayCalls.Clear();
    }

    internal void RaisePositionUpdate(double position)
    {
      PositionUpdate?.Invoke(position);
    }

    internal void RaiseSourceFinished()
    {
      SourceFinished?.Invoke();
    }
  }
}