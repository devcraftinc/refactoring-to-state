﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using Behavior;
using Contract;
using NUnit.Framework;
using static Behavior.Playback.State;

namespace Specification
{
  using Behaviors = TestFixtureAttribute;
  using Behavior = TestAttribute;
  using BeforeEach = SetUpAttribute;

  [Behaviors]
  public class PlaybackBehaviors
  {
    private MockPlaybackUI ui;
    private MockSource initialSource;
    private List<Playback.Ad> ads;
    private Playback playback;

    [BeforeEach]
    public void Setup()
    {
      ui = new MockPlaybackUI();
      initialSource = new MockSource("main");
      ads = new List<Playback.Ad>();
    }

    [Behavior]
    public void AdNotTriggeredWhilePlayingAnotherAd()
    {
      StartPlaybackWithAds();
      CrossPosition(AdPosition(0));

      CrossPosition(AdPosition(1));

      ThereShouldBeNoPlayCalls();
    }

    [Behavior]
    public void AdReturnsToOriginalPlaybackPosition()
    {
      StartPlaybackWithAds();

      var positionBeforeAd = PlayEntireAd(0);

      StateShouldBe(PlayingMainSource);
      OnlyPlayCallShouldBe(initialSource, positionBeforeAd);
    }

    [Behavior]
    public void BoundaryDetectionThreshold()
    {
      Assert.AreEqual(0.1d, Playback.BoundaryDetectionTolerance);
    }

    [Behavior]
    public void CannotFinishTwice()
    {
      StartPlayback();
      SourceFinished();

      Assert.That(SourceFinished, Throws.InvalidOperationException);
    }

    [Behavior]
    public void CannotSeekAfterFinish()
    {
      StartPlaybackWithAds();
      SourceFinished();

      Assert.That(() => PlayTo(AdPosition(2)), Throws.InvalidOperationException);
    }

    [Behavior]
    public void ExitAfterFinish()
    {
      StartPlayback();

      SourceFinished();

      Assert.AreEqual(1, ui.CloseCalls);
      StateShouldBe(Finished);
    }

    [Behavior]
    public void FastForwardDisabledDuringAdPlayback()
    {
      StartPlaybackWithAds();
      PlaybackSpeedIs(2d);

      CrossPosition(AdPosition(0));

      FastForwardShouldBeEnabled(false);
      PlaybackSpeedShouldBe(1);
    }

    [Behavior]
    public void FinishingAdReEnablesFastforward()
    {
      StartPlaybackWithAds();
      PlaybackSpeedIs(3d);

      PlayEntireAd(0);

      FastForwardShouldBeEnabled(true);
      PlaybackSpeedShouldBe(3d);
    }

    [Behavior]
    public void InitialStateIsPlaying()
    {
      StartPlayback();

      StateShouldBe(PlayingMainSource);
      OnlyPlayCallShouldBe(initialSource, 0);
      FastForwardShouldBeEnabled(true);
      PlaybackSpeedShouldBe(1);
    }

    [Behavior]
    public void LaunchAd()
    {
      StartPlaybackWithAds();

      CrossPosition(AdPosition(0));

      StateShouldBe(PlayingAd);
      OnlyPlayCallShouldBe(AdSource(0), 0);
    }

    [Behavior]
    public void SecondAdCanBePlayed()
    {
      StartPlaybackWithAds();
      PlayEntireAd(0);
      ForgetUIHistory();

      CrossPosition(AdPosition(1));

      StateShouldBe(PlayingAd);
      OnlyPlayCallShouldBe(AdSource(1), 0);
    }

    [Behavior]
    public void SkippingMultipleAdsInvokesBothInOrder()
    {
      StartPlaybackWithAds();
      PlayTo(AdPosition(0) - Playback.BoundaryDetectionTolerance);
      PlayTo(AdPosition(1) + Playback.BoundaryDetectionTolerance);
      ForgetUIHistory();

      SourceFinished();

      OnlyPlayCallShouldBe(AdSource(1), 0);
      StateShouldBe(PlayingAd);
      Assert.AreEqual(0, ui.CloseCalls);
    }

    private void PlaybackSpeedIs(double actual)
    {
      ui.PlaybackSpeed = actual;
    }

    private void PlaybackSpeedShouldBe(double expected)
    {
      Assert.AreEqual(expected, ui.PlaybackSpeed);
    }

    private void SourceFinished()
    {
      ForgetUIHistory();
      ui.RaiseSourceFinished();
    }

    private Source AdSource(int v)
    {
      return ads[v].Source;
    }

    private double AdPosition(int v)
    {
      return ads[v].Position;
    }

    private void StartPlaybackWithAds()
    {
      GivenAd(120.0);
      GivenAd(600.0);
      GivenAd(1200.0);
      StartPlayback();
    }

    private void GivenAd(double position)
    {
      ads.Add(Playback.Ad.GetInstance(new MockSource("ad @" + position), position));
    }

    private double CrossPosition(double threshold)
    {
      PlayTo(threshold - Playback.BoundaryDetectionTolerance);
      ForgetUIHistory();
      var newPosition = threshold + Playback.BoundaryDetectionTolerance;
      PlayTo(newPosition);
      return newPosition;
    }

    private void StateShouldBe(Playback.State expected)
    {
      Assert.AreEqual(expected, playback.CurrentState);
    }

    private void StartPlayback()
    {
      playback = Playback.Start(ui, initialSource, ads);
    }

    private void PlayTo(double position)
    {
      ui.RaisePositionUpdate(position);
    }

    private void ThereShouldBeNoPlayCalls()
    {
      Assert.AreEqual(0, ui.PlayCalls.Count);
    }

    private void ForgetUIHistory()
    {
      ui.Reset();
    }

    private void OnlyPlayCallShouldBe(Source expectedSource, double expectedPosition)
    {
      Assert.AreEqual(1, ui.PlayCalls.Count);
      var call = ui.PlayCalls[0];
      Assert.AreSame(expectedSource, call.source);
      Assert.AreEqual(expectedPosition, call.position);
    }

    private void FastForwardShouldBeEnabled(bool v)
    {
      Assert.AreEqual(v, ui.FastForwardEnabled);
    }

    private double PlayEntireAd(int ad)
    {
      var positionBeforeAd = CrossPosition(AdPosition(ad));
      SourceFinished();

      return positionBeforeAd;
    }
  }
}