/*
 * Copyright 2018 DevCraft, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package katas;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

class MockPlaybackUI extends UI {

	public static class Pair<T1, T2> {
		
		private T1 key;
		private T2 value;

		public Pair(T1 key, T2 value) {
			this.key = key;
			this.value = value;
		}
		
		public T1 getKey() {
			return key;
		}
		
		public T2 getValue() {
			return value;
		}
	}

	MockPlaybackUI() {
		playCalls = new ArrayList<>();
	}

	private List<Pair<Source, Double>> playCalls;

	public List<Pair<Source, Double>> getPlayCalls() {
		return playCalls;
	}

	private int closeCalls;

	public int getCloseCalls() {
		return closeCalls;
	}

	private boolean fastForwardEnabled;

	@Override
	public boolean getFastForwardEnabled() {
		return fastForwardEnabled;
	}

	@Override
	public void setFastForwardEnabled(boolean value) {
		fastForwardEnabled = value;
	}

	private double playbackSpeed;

	@Override
	public double getPlaybackSpeed() {
		return playbackSpeed;
	}

	@Override
	public void setPlaybackSpeed(double value) {
		playbackSpeed = value;
	}

	@Override
	public void play(Source source, double seconds) {
		playCalls.add(new Pair<>(source, seconds));
	}

	@Override
	public void close() {
		closeCalls++;
	}

	private List<Consumer<Double>> positionUpdateHandlers = new ArrayList<>();

	@Override
	public void watchPositionUpdate(Consumer<Double> handler) {
		positionUpdateHandlers.add(handler);
	}

	public void raisePositionUpdate(double position) {
		for (Consumer<Double> handler : positionUpdateHandlers) {
			handler.accept(position);
		}
	}

	private List<Runnable> sourceFinishedHandlers = new ArrayList<>();

	@Override
	public void watchSourceFinished(Runnable handler) {
		sourceFinishedHandlers.add(handler);
	}

	public void raiseSourceFinished() {
		for (Runnable handler : sourceFinishedHandlers) {
			handler.run();
		}
	}

	public void reset() {
		closeCalls = 0;
		playCalls.clear();
	}
}
