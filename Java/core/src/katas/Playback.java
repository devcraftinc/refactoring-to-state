/*
 * Copyright 2018 DevCraft, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package katas;

public class Playback {
    public enum State {
        PlayingMainSource,
        PlayingAd,
        Finished
    }

    public static class Ad {
        private Ad(Source source, double position) {
            this.source = source;
            this.position = position;
        }

        private Source source;

        public Source getSource() {
            return source;
        }

        private double position;

        public double getPosition() {
            return position;
        }

        public static Ad getInstance(Source source, double position) {
            return new Ad(source, position);
        }
    }

    private final Iterable<Ad> ads;
    private final Source source;

    private final UI ui;
    private double currentPosition;
    private double currentSpeed;
    private Ad playingAd;
    private State state;

    private Playback(UI ui, Source source, Iterable<Ad> ads) {
        this.ui = ui;
        this.ui.watchPositionUpdate(this::onPositionUpdate);
        this.ui.watchSourceFinished(this::onSourceFinished);
        this.source = source;
        this.ads = ads;
        currentSpeed = 1d;
        onPlayMainSource();
    }

    public State getCurrentState() {
        return state;
    }

    public static double getBoundaryDetectionTolerance() {
        return 0.1d;
    }

    public static Playback start(UI ui, Source source, Iterable<Ad> ads) {
        return new Playback(ui, source, ads);
    }

    private void onPositionUpdate(double newPosition) {
        switch (state) {
            case PlayingMainSource:
                playNextAd(currentPosition, newPosition);
                currentPosition = newPosition;
                break;
            case PlayingAd:
                break;
            case Finished:
                throw new IllegalStateException();
        }
    }

    private void onSourceFinished() {
        switch (state) {
            case PlayingAd:
                if (!playNextAd(playingAd.getPosition(), currentPosition))
                    onPlayMainSource();
                break;
            case PlayingMainSource:
                ui.close();
                state = State.Finished;
                break;
            case Finished:
                throw new IllegalStateException();
        }
    }

    private void onPlayMainSource() {
        ui.play(source, currentPosition);
        ui.setFastForwardEnabled(true);
        ui.setPlaybackSpeed(currentSpeed);
        state = State.PlayingMainSource;
    }

    private boolean playNextAd(double beforePosition, double afterPosition) {
        for (Ad ad : ads)
            if (beforePosition < ad.getPosition() && afterPosition > ad.getPosition()) {
                ui.play(ad.getSource(), 0);
                ui.setFastForwardEnabled(false);
                currentSpeed = ui.getPlaybackSpeed();
                ui.setPlaybackSpeed(1);
                state = State.PlayingAd;
                playingAd = ad;
                return true;
            }

        return false;
    }
}

