/*
 * Copyright 2018 DevCraft, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


package katas ;

import static katas.Playback.State.Finished;
import static katas.Playback.State.PlayingAd;
import static katas.Playback.State.PlayingMainSource;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import katas.MockPlaybackUI.Pair;

public class PlaybackBehaviors
  {
    private MockPlaybackUI ui;
    private MockSource initialSource;
    private ArrayList<Playback.Ad> ads;
    private Playback playback;

    @Before
    public void setup()
    {
      ui = new MockPlaybackUI();
      initialSource = new MockSource("main");
      ads = new ArrayList<>();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void adNotTriggeredWhilePlayingAnotherAd()
    {
      startPlaybackWithAds();
      crossPosition(adPosition(0));

      crossPosition(adPosition(1));

      thereShouldBeNoPlayCalls();
    }

    @Test
    public void adReturnsToOriginalPlaybackPosition()
    {
      startPlaybackWithAds();

      double positionBeforeAd = playEntireAd(0);

      stateShouldBe(PlayingMainSource);
      onlyPlayCallShouldBe(initialSource, positionBeforeAd);
    }

    @Test
    public void boundaryDetectionThreshold()
    {
      assertEquals(0.1d, Playback.getBoundaryDetectionTolerance(), 0.0000001d);
    }

    @Test
    public void cannotFinishTwice()
    {
      startPlayback();
      sourceFinished();

      thrown.expect(IllegalStateException.class);

      sourceFinished();
    }

    @Test
    public void cannotSeekAfterFinish()
    {
      startPlaybackWithAds();
      sourceFinished();

      thrown.expect(IllegalStateException.class);

      playTo(adPosition(2));
    }

    @Test
    public void exitAfterFinish()
    {
      startPlayback();

      sourceFinished();

      assertEquals(1, ui.getCloseCalls());
      stateShouldBe(Finished);
    }

    @Test
    public void fastForwardDisabledDuringAdPlayback()
    {
      startPlaybackWithAds();
      playbackSpeedIs(2d);

      crossPosition(adPosition(0));

      fastForwardShouldBeEnabled(false);
      playbackSpeedShouldBe(1);
    }

    @Test
    public void finishingAdReEnablesFastForward()
    {
      startPlaybackWithAds();
      playbackSpeedIs(3d);

      playEntireAd(0);

      fastForwardShouldBeEnabled(true);
      playbackSpeedShouldBe(3d);
    }

    @Test
    public void initialStateIsPlaying()
    {
      startPlayback();

      stateShouldBe(PlayingMainSource);
      onlyPlayCallShouldBe(initialSource, 0);
      fastForwardShouldBeEnabled(true);
      playbackSpeedShouldBe(1);
    }

    @Test
    public void launchAd()
    {
      startPlaybackWithAds();

      crossPosition(adPosition(0));

      stateShouldBe(PlayingAd);
      onlyPlayCallShouldBe(adSource(0), 0);
    }

    @Test
    public void secondAdCanBePlayed()
    {
      startPlaybackWithAds();
      playEntireAd(0);
      forgetUIHistory();

      crossPosition(adPosition(1));

      stateShouldBe(PlayingAd);
      onlyPlayCallShouldBe(adSource(1), 0);
    }

    @Test
    public void skippingMultipleAdsInvokesBothInOrder()
    {
      startPlaybackWithAds();
      playTo(adPosition(0) - Playback.getBoundaryDetectionTolerance());
      playTo(adPosition(1) + Playback.getBoundaryDetectionTolerance());
      forgetUIHistory();

      sourceFinished();

      onlyPlayCallShouldBe(adSource(1), 0);
      stateShouldBe(PlayingAd);
      assertEquals(0, ui.getCloseCalls());
    }

    private void playbackSpeedIs(double actual)
    {
      ui.setPlaybackSpeed(actual);
    }

    private void playbackSpeedShouldBe(double expected)
    {
      assertEquals(expected, ui.getPlaybackSpeed(), 0.0000000001d);
    }

    private void sourceFinished()
    {
      forgetUIHistory();
      ui.raiseSourceFinished();
    }

    private Source adSource(int v)
    {
      return ads.get(v).getSource();
    }

    private double adPosition(int v)
    {
      return ads.get(v).getPosition();
    }

    private void startPlaybackWithAds()
    {
      givenAd(120.0);
      givenAd(600.0);
      givenAd(1200.0);
      startPlayback();
    }

    private void givenAd(double position)
    {
      ads.add(Playback.Ad.getInstance(new MockSource("ad @" + position), position));
    }

    private double crossPosition(double threshold)
    {
      playTo(threshold - Playback.getBoundaryDetectionTolerance());
      forgetUIHistory();
      double newPosition = threshold + Playback.getBoundaryDetectionTolerance();
      playTo(newPosition);
      return newPosition;
    }

    private void stateShouldBe(Playback.State expected)
    {
      assertEquals(expected, playback.getCurrentState());
    }

    private void startPlayback()
    {
      playback = Playback.start(ui, initialSource, ads);
    }

    private void playTo(double position)
    {
      ui.raisePositionUpdate(position);
    }

    private void thereShouldBeNoPlayCalls()
    {
      assertEquals(0, ui.getPlayCalls().size());
    }

    private void forgetUIHistory()
    {
      ui.reset();
    }

    private void onlyPlayCallShouldBe(Source expectedSource, double expectedPosition)
    {
      assertEquals(1, ui.getPlayCalls().size());
      Pair<Source, Double> call = ui.getPlayCalls().get(0);
      assertSame(expectedSource, call.getKey());
      assertEquals(expectedPosition, call.getValue(), 0.0000000000001d);
    }

    private void fastForwardShouldBeEnabled(boolean v)
    {
      assertEquals(v, ui.getFastForwardEnabled());
    }

    private double playEntireAd(int ad)
    {
      double positionBeforeAd = crossPosition(adPosition(ad));
      sourceFinished();

      return positionBeforeAd;
    }
  }

