﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


public class Playback {
  public enum PlaybackError: Error {
    case InvalidOperation
  }

  public enum State {
    case PlayingMainSource
    case PlayingAd
    case Finished
  }

  public class Ad {
    private init(_ source: Source, _  position: Double) {
      self.source = source
      self.position = position
    }

    public let source: Source
    public let position: Double

    public static func getInstance(_ source: Source, _ position: Double) -> Ad {
      return Ad(source, position)
    }
  }

  private var ads: [Ad]
  private var source: Source

  private var ui: UI
  private var currentPosition: Double
  private var currentSpeed: Double
  private var playingAd: Ad?
  private var state: State

  private init(_ ui: UI, _ source: Source, _ ads: [Ad]) {
    self.ui = ui
    self.source = source
    self.ads = ads
    self.currentSpeed = 1
    self.state = State.PlayingMainSource
    self.currentPosition = 0
    self.playingAd = nil

    onPlayMainSource()

    self.ui.watchPositionUpdated(onPositionUpdate)
    self.ui.watchSourceFinished(onSourceFinished)
  }

  public var currentState: State {
    return state
  }

  public static let boundaryDetectionTolerance = Double(0.1)

  public static func start(_ ui: UI, _ source: Source, _ ads: [Ad]) -> Playback {
    return Playback(ui, source, ads)
  }

  private func onPositionUpdate(_ newPosition: Double) throws {
    switch (state) {
    case State.PlayingMainSource:
      var _ = playNextAd(currentPosition, newPosition)
      currentPosition = newPosition
      break
    case State.PlayingAd:
      break
    case State.Finished:
      throw PlaybackError.InvalidOperation
    }
  }

  private func onSourceFinished() throws {

    switch (state) {
    case State.PlayingAd:
      if (!playNextAd(playingAd!.position, currentPosition)) {
        onPlayMainSource()
      }
      break
    case State.PlayingMainSource:
      ui.close()
      state = State.Finished
      break
    case State.Finished:
      throw PlaybackError.InvalidOperation
    }
  }

  private func onPlayMainSource() {
    ui.play(source, currentPosition)
    ui.fastForwardEnabled = true
    ui.playbackSpeed = currentSpeed
    state = State.PlayingMainSource
  }

  private func playNextAd(_ beforePosition: Double, _ afterPosition: Double) -> Bool {
    for ad in ads {
      if (beforePosition < ad.position && afterPosition > ad.position) {
        ui.play(ad.source, 0)
        ui.fastForwardEnabled = false
        currentSpeed = ui.playbackSpeed
        ui.playbackSpeed = 1
        state = State.PlayingAd
        playingAd = ad
        return true
      }

    }

    return false
  }
}
