﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Katas_SwitchToState1

internal class MockPlaybackUI: UI {
    private var onPositionUpdated: [(Double) throws -> Void] = []
    private var onSourceFinished: [() throws -> Void] = []
    public var playCalls: [(Source, Double)] = []
    public var closeCalls: Int = 0
    public var fastForwardEnabled: Bool = false
    public var playbackSpeed: Double = 0

    public func play(_ source: Source, _ seconds: Double) {
        playCalls.append((source, seconds))
    }

    public func close() {
        closeCalls += 1
    }

    public func watchPositionUpdated(_ handler: @escaping (Double) throws -> Void) {
        onPositionUpdated.append(handler)
    }

    public func watchSourceFinished(_ handler: @escaping () throws -> Void) {
        onSourceFinished.append(handler)
    }

    internal func reset() {
        playCalls.removeAll()
    }

    internal func raisePositionUpdate(_ position: Double) throws {
        for handler in onPositionUpdated {
            try handler(position)
        }
    }

    internal func raiseSourceFinished() throws {
        for handler in onSourceFinished {
            try handler()
        }
    }
}