﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import XCTest
import Katas_SwitchToState1

public class PlaybackTests: XCTestCase {
  private var ui: MockPlaybackUI
  private var initialSource: MockSource
  private var ads: [Playback.Ad]
  private var playback: Playback

  public required init(name: String, testClosure: @escaping(XCTestCase) throws -> Void) {
    ui = MockPlaybackUI()
    initialSource = MockSource()
    ads = []
    playback = Playback.start(ui, initialSource, ads)
    super.init(name: name, testClosure: testClosure)
  }

  public override func setUp() {
    ui = MockPlaybackUI()
    initialSource = MockSource()
    ads = []
  }

  public func testAdNotTriggeredWhilePlayingAnotherAd() throws {
    startPlaybackWithAds()
    var _ = try crossPosition(adPosition(0))

    _ = try crossPosition(adPosition(1))

    thereShouldBeNoPlayCalls()
  }

  public func testAdReturnsToOriginalPlaybackPosition() throws {
    startPlaybackWithAds()

    let positionBeforeAd = try playEntireAd(0)

    stateShouldBe(Playback.State.PlayingMainSource)
    onlyPlayCallShouldBe(initialSource, positionBeforeAd)
  }

  public func testBoundaryDetectionThreshold() {
    XCTAssertEqual(0.1, Playback.boundaryDetectionTolerance)
  }

  public func testCannotFinishTwice() throws {
    startPlayback()
    try sourceFinished()

    XCTAssertThrowsError(try sourceFinished()) {
      error in
      XCTAssertEqual(error as? Playback.PlaybackError, Playback.PlaybackError.InvalidOperation)
    }
  }

  public func testCannotSeekAfterFinish() throws {
    startPlaybackWithAds()
    try sourceFinished()


    XCTAssertThrowsError(try playTo(adPosition(2))) {
      error in
      XCTAssertEqual(error as? Playback.PlaybackError, Playback.PlaybackError.InvalidOperation)
    }
  }

  public func testExitAfterFinish() throws {
    startPlayback()

    try sourceFinished()

    XCTAssertEqual(1, ui.closeCalls)
    stateShouldBe(Playback.State.Finished)
  }

  public func testFastForwardDisabledDuringAdPlayback() throws {
    startPlaybackWithAds()
    playbackSpeedIs(2)

    var _ = try crossPosition(adPosition(0))

    fastForwardShouldBeEnabled(false)
    playbackSpeedShouldBe(1)
  }

  public func testFinishingAdReEnablesFastforward() throws {
    startPlaybackWithAds()
    playbackSpeedIs(3)

    var _ = try playEntireAd(0)

    fastForwardShouldBeEnabled(true)
    playbackSpeedShouldBe(3)
  }

  public func testInitialStateIsPlaying() {
    startPlayback()

    stateShouldBe(Playback.State.PlayingMainSource)
    onlyPlayCallShouldBe(initialSource, 0)
    fastForwardShouldBeEnabled(true)
    playbackSpeedShouldBe(1)
  }

  public func testLaunchAd() throws {
    startPlaybackWithAds()

    var _ = try crossPosition(adPosition(0))

    stateShouldBe(Playback.State.PlayingAd)
    onlyPlayCallShouldBe(adSource(0), 0)
  }

  public func testSecondAdCanBePlayed() throws {
    startPlaybackWithAds()
    var _ = try playEntireAd(0)
    forgetUIHistory()

    _ = try crossPosition(adPosition(1))

    stateShouldBe(Playback.State.PlayingAd)
    onlyPlayCallShouldBe(adSource(1), 0)
  }

  public func testSkippingMultipleAdsInvokesBothInOrder() throws {
    var _ = startPlaybackWithAds()
    try playTo(adPosition(0) - Playback.boundaryDetectionTolerance)
    try playTo(adPosition(1) + Playback.boundaryDetectionTolerance)
    _ = forgetUIHistory()

    _ = try sourceFinished()

    onlyPlayCallShouldBe(adSource(1), 0)
    stateShouldBe(Playback.State.PlayingAd)
    XCTAssertEqual(0, ui.closeCalls)
  }

  private func playbackSpeedIs(_ actual: Double) {
    ui.playbackSpeed = actual
  }

  private func playbackSpeedShouldBe(_ expected: Double) {
    XCTAssertEqual(expected, ui.playbackSpeed)
  }

  private func sourceFinished() throws {
    var _ = forgetUIHistory()
    try ui.raiseSourceFinished()
  }

  private func adSource(_ v: Int) -> Source {
    return ads[v].source
  }

  private func adPosition(_ v: Int) -> Double {
    return ads[v].position
  }

  private func startPlaybackWithAds() {
    var _ = givenAd(120.0)
    _ = givenAd(600.0)
    _ = givenAd(1200.0)
    startPlayback()
  }

  private func givenAd(_ position: Double) {
    ads.append(Playback.Ad.getInstance(MockSource(), position))
  }

  private func crossPosition(_ threshold: Double) throws -> Double {
    try playTo(threshold - Playback.boundaryDetectionTolerance)
    forgetUIHistory()
    let newPosition = threshold + Playback.boundaryDetectionTolerance
    try playTo(newPosition)
    return newPosition
  }

  private func stateShouldBe(_ expected: Playback.State) {
    XCTAssertEqual(expected, playback.currentState)
  }

  private func startPlayback() {
    playback = Playback.start(ui, initialSource, ads)
  }

  private func playTo(_ position: Double) throws {
    try ui.raisePositionUpdate(position)
  }

  private func thereShouldBeNoPlayCalls() {
    XCTAssertEqual(0, ui.playCalls.count)
  }

  private func forgetUIHistory() {
    ui.reset()
  }

  private func onlyPlayCallShouldBe(_ expectedSource: Source, _ expectedPosition: Double) {
    XCTAssertEqual(1, ui.playCalls.count)
    let (source, position) = ui.playCalls[0]
    XCTAssert(expectedSource === source)
    XCTAssertEqual(expectedPosition, position)
  }

  private func fastForwardShouldBeEnabled(_ v: Bool) {
    XCTAssertEqual(v, ui.fastForwardEnabled)
  }

  private func playEntireAd(_ ad: Int) throws -> Double {
    let positionBeforeAd = try crossPosition(adPosition(ad))
    try sourceFinished()

    return positionBeforeAd
  }

  static let allTests = [
    ("Ads not triggered during other ads", testAdNotTriggeredWhilePlayingAnotherAd),
    ("Ads return to original place in main source", testAdReturnsToOriginalPlaybackPosition),
    ("Boundary detection threshold", testBoundaryDetectionThreshold),
    ("Cannot finish twice", testCannotFinishTwice),
    ("Cannot seek after finish", testCannotSeekAfterFinish),
    ("Cannot end source after finish", testExitAfterFinish),
    ("Cannot fast forward during ad playback", testFastForwardDisabledDuringAdPlayback),
    ("Fast forward re-enabled after ad finishes", testFinishingAdReEnablesFastforward),
    ("Starts off attempting to play main source", testInitialStateIsPlaying),
    ("Launches ad when threshold is met", testLaunchAd),
    ("Launches second ad after first finishes", testSecondAdCanBePlayed),
    ("Launches long skip invokes multiple ads back-to-back", testSkippingMultipleAdsInvokesBothInOrder),
  ]
}
