﻿import { Source } from "./Source";
import { PlaybackAd } from "./PlaybackAd";
import { UI } from "./UI";
import { PlaybackState } from "./PlaybackState";
import { InvalidOperationException } from "./InvalidOperationException";

export class Playback {
    private readonly ads: PlaybackAd[];
    private readonly source: Source;

    private readonly ui: UI;
    private currentPosition: number = 0;
    private currentSpeed: number;
    private playingAd: PlaybackAd;
    private state: PlaybackState;

    private constructor(ui: UI, source: Source, ads: PlaybackAd[]) {
        this.ui = ui;
        this.ui.WatchPositionUpdated(p => this.OnPositionUpdate(p));
        this.ui.WatchSourceFinished(() => this.OnSourceFinished());
        this.source = source;
        this.ads = ads;
        this.currentSpeed = 1;
        this.OnPlayMainSource();
    }

    public get CurrentState(): PlaybackState { return this.state; }
    public static get BoundaryDetectionTolerance(): number { return 0.1; }


    public static Start(ui: UI, source: Source, ads: PlaybackAd[]): Playback {
        return new Playback(ui, source, ads);
    }

    private OnPositionUpdate(newPosition: number) {
        switch (this.state) {
            case PlaybackState.PlayingMainSource:
                this.PlayNextAd(this.currentPosition, newPosition);
                this.currentPosition = newPosition;
                break;
            case PlaybackState.PlayingAd:
                break;
            case PlaybackState.Finished:
                throw new InvalidOperationException();
        }
    }

    private OnSourceFinished() {
        switch (this.state) {
            case PlaybackState.PlayingAd:
                if (!this.PlayNextAd(this.playingAd.position, this.currentPosition))
                    this.OnPlayMainSource();
                break;
            case PlaybackState.PlayingMainSource:
                this.ui.Close();
                this.state = PlaybackState.Finished;
                break;
            case PlaybackState.Finished:
                throw new InvalidOperationException();
        }
    }

    private OnPlayMainSource() {
        this.ui.Play(this.source, this.currentPosition);
        this.ui.FastForwardEnabled = true;
        this.ui.PlaybackSpeed = this.currentSpeed;
        this.state = PlaybackState.PlayingMainSource;
    }

    private PlayNextAd(beforePosition: number, afterPosition: number): boolean {
        for (let ad of this.ads)
            if (beforePosition < ad.position && afterPosition > ad.position) {
                this.ui.Play(ad.source, 0);
                this.ui.FastForwardEnabled = false;
                this.currentSpeed = this.ui.PlaybackSpeed;
                this.ui.PlaybackSpeed = 1;
                this.state = PlaybackState.PlayingAd;
                this.playingAd = ad;
                return true;
            }

        return false;
    }
}