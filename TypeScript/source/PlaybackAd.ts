﻿import { Source } from "./Source";

export class PlaybackAd {
    public readonly source: Source;
    public readonly position: number;

    private constructor(source: Source, position: number) {
        this.source = source;
        this.position = position;
    }

    public static GetInstance(source: Source, position: number): PlaybackAd {
        return new PlaybackAd(source, position);
    }
}