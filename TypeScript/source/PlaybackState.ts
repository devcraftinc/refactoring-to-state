﻿export enum PlaybackState {
    PlayingMainSource,
    Finished,
    PlayingAd
}