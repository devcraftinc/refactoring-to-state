﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import assert = require('assert');
import { Playback } from "../source/Playback";
import { PlaybackState } from "../source/PlaybackState";
import { PlaybackAd } from "../source/PlaybackAd";
import { Source } from "../source/Source";
import { MockSource } from "./MockSource";
import { MockPlaybackUI } from "./MockPlaybackUI";

var ui: MockPlaybackUI;
var initialSource: MockSource;
var ads: PlaybackAd[];
var playback: Playback;

function setUp() {
    ui = new MockPlaybackUI();
    initialSource = new MockSource();
    ads = [];
}
export function testItDoesNotTriggerAdsWhilePlayingAnotherAd() {
    setUp();
    startPlaybackWithAds();
    crossPosition(adPosition(0));

    crossPosition(adPosition(1));

    thereShouldBeNoPlayCalls();
}

export function testItReturnsToOriginalPlaybackAfterAnAd() {
    setUp();
    startPlaybackWithAds();

    var positionBeforeAd = playEntireAd(0);

    stateShouldBe(PlaybackState.PlayingMainSource);
    onlyPlayCallShouldBe(initialSource, positionBeforeAd);
}

export function testItIsAccurateToWithinAnAcceptableFractionOfASecond() {
    setUp();
    assert.equal(0.1, Playback.BoundaryDetectionTolerance);
}

export function itCannotFinishTwice() {
    setUp();
    startPlayback();
    sourceFinished();

    assert.throws(sourceFinished);
}

export function itCannotSeekAfterFinish() {
    setUp();
    startPlaybackWithAds();
    sourceFinished();

    assert.throws(() => playTo(adPosition(2)));
}

export function itExitsAfterFinish() {
    setUp();
    startPlayback();

    sourceFinished();

    assert.equal(1, ui.CloseCalls, "number of close calls");
    stateShouldBe(PlaybackState.Finished);
}

export function itDoesNotAllowFastForwardWhenPlayingAnAd() {
    setUp();
    startPlaybackWithAds();
    playbackSpeedIs(2);

    crossPosition(adPosition(0));

    fastForwardShouldBeEnabled(false);
    playbackSpeedShouldBe(1);
}

export function itRestoresFastForwardOrSlowMoAfterAnAdIsFinished() {
    setUp();
    startPlaybackWithAds();
    playbackSpeedIs(3);

    playEntireAd(0);

    fastForwardShouldBeEnabled(true);
    playbackSpeedShouldBe(3);
}

export function itStartsInThePlayingState() {
    setUp();
    startPlayback();

    stateShouldBe(PlaybackState.PlayingMainSource);
    onlyPlayCallShouldBe(initialSource, 0);
    fastForwardShouldBeEnabled(true);
    playbackSpeedShouldBe(1);
}

export function itLaunchesAds() {
    setUp();
    startPlaybackWithAds();

    crossPosition(adPosition(0));

    stateShouldBe(PlaybackState.PlayingAd);
    onlyPlayCallShouldBe(adSource(0), 0);
}

export function itPlaysASecondAdAfterTheFirstOneFinishes() {
    setUp();
    startPlaybackWithAds();
    playEntireAd(0);
    forgetUIHistory();

    crossPosition(adPosition(1));

    stateShouldBe(PlaybackState.PlayingAd);
    onlyPlayCallShouldBe(adSource(1), 0);
}

export function itPlaysMultipleAdsIfBothAreSkipped() {
    setUp();
    startPlaybackWithAds();
    playTo(adPosition(0) - Playback.BoundaryDetectionTolerance);
    playTo(adPosition(1) + Playback.BoundaryDetectionTolerance);
    forgetUIHistory();

    sourceFinished();

    onlyPlayCallShouldBe(adSource(1), 0);
    stateShouldBe(PlaybackState.PlayingAd);
}

function playbackSpeedIs(actual: number) {
    ui.PlaybackSpeed = actual;
}

function playbackSpeedShouldBe(expected: number) {
    assert.strictEqual(expected, ui.PlaybackSpeed, "playback speed");
}

function sourceFinished() {
    forgetUIHistory();
    ui.RaiseSourceFinished();
}

function adSource(v: number): Source {
    return ads[v].source;
}

function adPosition(v: number): number {
    return ads[v].position;
}

function startPlaybackWithAds() {
    givenAd(120.0);
    givenAd(600.0);
    givenAd(1200.0);
    startPlayback();
}

function givenAd(position: number) {
    ads.push(PlaybackAd.GetInstance(new MockSource(), position));
}

function crossPosition(threshold: number): number {
    playTo(threshold - Playback.BoundaryDetectionTolerance);
    forgetUIHistory();
    var newPosition = threshold + Playback.BoundaryDetectionTolerance;
    playTo(newPosition);
    return newPosition;
}

function stateShouldBe(expected: PlaybackState) {
    assert.equal(expected, playback.CurrentState, "current state");
}

function startPlayback() {
    playback = Playback.Start(ui, initialSource, ads);
}

function playTo(position: number) {
    ui.RaisePositionUpdate(position);
}

function thereShouldBeNoPlayCalls() {
    assert.equal(0, ui.PlayCalls.length, "no play calls");
}

function forgetUIHistory() {
    ui.Reset();
}

function onlyPlayCallShouldBe(expectedSource: Source, expectedPosition: number) {
    assert.equal(1, ui.PlayCalls.length, "number of play calls");
    let [source, position] = ui.PlayCalls[0];
    assert.equal(expectedSource, source, "source for play");
    assert.equal(expectedPosition, position, "position for play");
}

function fastForwardShouldBeEnabled(v: boolean) {
    assert.equal(v, ui.FastForwardEnabled, "fast-forward enablement");
}

function playEntireAd(ad: number): number {
    var positionBeforeAd = crossPosition(adPosition(ad));
    sourceFinished();

    return positionBeforeAd;
}


